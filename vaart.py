import art
import clize
import pyperclip


def vaart(string: str = "das labor", *, font: str = "roman", top_lines: int = 2, line_spread: int = 4, char_spread: int = 1, clipboard: bool = True):
    """Prints a string as vertical ASCII art

    https://codeberg.org/andiandi/vertical-aart

    :param string: String to print
    :param font: Font name from art library
    :param top_lines: Number of lines added to top
    :param line_spread: Factor for multiplying each line
    :param char_spread: Factor for multiplying each character
    :param clipboard: Insert text to clipboard
    """

    # validate arguments
    if top_lines < 0:
        raise ArgumentError("Argument top-lines must be >=0")

    if line_spread < 1:
        raise ArgumentError("Argument line-spread must be >0")

    if char_spread < 1:
        raise ArgumentError("Argument char-spread must be >0")

    # get aart lines split as list
    horizontal_text = art.text2art(string + " ", font).splitlines()

    # get maximum line character count
    chars_per_line = max([len(x) for x in horizontal_text])

    # list for the lines
    lines = list()

    # additional lines
    top_lines_list = [" " for x in range(top_lines)]

    for n in range(chars_per_line):
        # take n-th char from each line, as list
        line = [x[n]*line_spread for x in horizontal_text]

        # join list elements to form the line to a string
        string = "".join(top_lines_list + line)

        # put new line in front of list to accomodate reading direction
        for x in range(char_spread):
            lines.insert(0, string)

    # print each line
    for line in lines:
        print(f"{line}")

    # if requested, join all lines to a single string before copying to clipboard
    if clipboard:
        string = "\n".join(lines)
        pyperclip.copy(string)


if __name__ == '__main__':
    clize.run(vaart)
